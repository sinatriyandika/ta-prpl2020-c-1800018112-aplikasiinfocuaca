	<?php
	include "models/m_anginsuhu.php";

	$agnsh = new anginsuhu($connection);
	?>
		<div class="row">
          <div class="col-lg-12">
            <h1>Kecepatan angin dan Suhu <small></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> info</a></li>
            </ol>
          </div>
        </div>

        <div class="row">
        	<div class="col-lg-12">	
        		<div class="table-responsive">
        			<table class="table table-bordered table-hover table-striped">
        				<tr>
        					<th>No.</th>
        					<th>Nama Daerah</th>
        					<th>kecepatan angin</th>
        					<th>suhu</th>
        					<th>kelembaban</th>
        					<th>jam</th>
        					<th>tanggal</th>
        					<th>Prediksi</th>
        					<th>Opsi</th>
        				</tr>
        				<?php
        				$no = 1;
        				$tampil = $agnsh->tampil();
        				while ($data = $tampil->fetch_object()) {
        				?>	
        				<tr>
        					<td align="center"><?php echo $no++."."; ?></td>
        					<td><?php echo $data->nama_daerah; ?></td>
        					<td><?php echo $data->kecepatan_angin; ?></td>
        					<td><?php echo $data->suhu; ?></td>
        					<td><?php echo $data->kelembaban; ?></td>
        					<td><?php echo $data->jam; ?></td>
        					<td><?php echo $data->tanggal; ?></td>
        					<td>
                                <img src="assets/img/gmbrprdks<?php echo $data->Prediksi; ?>" width="80px">
                                </td>
        					<td align="center">
        						<button class="btn-info btn -xs"><i class="fa fa-edit"></i> Edit</button>
        						<button class="btn-danger btn -xs"><i class="fa fa-trash-o"></i> Hapus</button>
        					</td>
        				</tr>
        				<?php
        				}	?>
        			</table>
        		</div>

                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data Cuaca</button>

                <div id="tambah" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Tambah Data</h4>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label class="control-label" for="nm_daerah">Nama Daerah</label>
                                        <input type="text" name="nm_daerah" class="form-control" id="nm_daerah" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="kcpt_agn">Kecepatan angin</label>
                                        <input type="text" name="kcpt_agn" class="form-control" id="kcpt_agn" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="sh">suhu</label>
                                        <input type="text" name="sh" class="form-control" id="sh" required>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label" for="klm_bbn">Kelembaban</label>
                                        <input type="text" name="klm_bbn" class="form-control" id="klm_bbn" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="j_m">jam</label>
                                        <input type="time" name="j_m" class="form-control" id="j_m" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="tgl">tanggal</label>
                                        <input type="date" name="tgl" class="form-control" id="tgl" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="prdks">Prediksi</label>
                                        <input type="file" name="prdks" class="form-control" id="prdks" required>
                                    </div>                                               
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <input type="submit" class="btn btn-success" name="tambah" value="Simpan">
                                </div>    
                            </form>
                            <?php
                            if(@$_POST['tambah']) {
                                $nm_daerah = $connection->conn->real_escape_string($_POST['nm_daerah']);
                                $kcpt_agn = $connection->conn->real_escape_string($_POST['kcpt_agn']);
                                $sh = $connection->conn->real_escape_string($_POST['sh']);
                                $klm_bbn = $connection->conn->real_escape_string($_POST['klm_bbn']);
                                $j_m = $connection->conn->real_escape_string($_POST['j_m']);
                                $tgl = $connection->conn->real_escape_string($_POST['tgl']);

                                $extensi = explode(".", $_FILES['prdks'] ['name']);
                                $prdks = "gmbr-".round(microtime(true)).".".end($extensi);
                                $sumber = $_FILES['prdks'] ['tmp_name'];
                                $upload = move_uploaded_file($sumber, "assets/img/gmbrprdks/".$prdks);
                                if($upload) {
                                    $agnsh->tambah($nm_daerah, $kcpt_agn, $sh, $klm_bbn, $j_m, $tgl, $prdks);
                                    header("location: ?page=anginsuhu");
                                }else{
                                    echo "<script>alert('Upload gambar gagal!')</script>";
                                }
                            }
                            ?>
                        </div>
                    </div>    
                </div>
                

        	</div>	
        </div>